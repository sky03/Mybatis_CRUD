package xyz.sky03.test;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import xyz.sky03.dao.UserDao;
import xyz.sky03.domain.User;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.List;

/**
 * Mybatis入门
 */
public class MybatisTest {

    private InputStream is;
    private SqlSession session;
    private UserDao userDao;

    @Before
    public void init() throws IOException {
        //1.读取配置文件
        is = Resources.getResourceAsStream("SqlMapConfig.xml");
        //2.创建SqlSessionFactory工厂（构建者模式）
        SqlSessionFactoryBuilder builder=new SqlSessionFactoryBuilder();
        SqlSessionFactory factory=builder.build(is);//builder就是构建者
        //3.使用工厂生产SqlSession对象
        session= factory.openSession();
        //4.使用SqlSession创建Dao接口的代理对象
        userDao = session.getMapper(UserDao.class);
    }

    @After
    public void destroy() throws IOException {
        session.commit();
        //6.释放资源
        session.close();
        is.close();
    }

    /**
     * 保存用户
     */
    @Test
    public void findAll() {
        //5.使用代理对象执行方法
        List<User> users = userDao.findAll();
        for (User user : users){
            System.out.println(user);
        }
    }

    @Test
    public void saveUser(){
        User user=new User();
        user.setUsername("亚索");
        user.setSex("男");
        user.setAddress("艾欧尼亚");
        user.setBirthday(new Date());
        userDao.saveUser(user);
    }
    @Test
    public void updateUser(){
        User user=new User();
        user.setId(50);
        user.setUsername("锐雯");
        user.setSex("女");
        user.setAddress("诺克萨斯");
        user.setBirthday(new Date());
        userDao.updateUser(user);
    }

    @Test
    public void deleteUser(){
        userDao.deleteUser(46);
    }
}
